import React, { useEffect , useState} from 'react';
import parse from 'html-react-parser';
import axios from 'axios';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import "../index.css";

const Actualite = () => {
    const  [page, setPage] = useState();
    useEffect(() => {
        axios.get('https://g1.esiee-it.o3creative.fr/wp-json/wp/v2/posts/?acf_format=standard')
        .then(function (response){
            //handle success
            console.log(response)
            setPage(response.data)
        });
    }, []);

    if(!page) return null;
    return (
        <div >
            
            <h1> ACTUALITES </h1>
    {
        page.map((item_data, index)=>{
            return <div className="styleCard">
                <Card key={`data_&{index}`}>
                <Card.Img variant="top" className='img_a' src={item_data.acf.image_actualites}/>
                <Card.Body>
                    <Card.Title >{item_data.acf.titre}</Card.Title>
                    <Card.Text>
                    {item_data.acf.description}
                    </Card.Text>
                    <Button variant="primary">En savoir plus</Button>
                </Card.Body>
                </Card>
                </div>
        })
       
    }
                
        </div>
    );
};

export default Actualite;