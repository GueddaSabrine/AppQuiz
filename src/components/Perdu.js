import React, { useEffect , useState} from 'react';
import parse from 'html-react-parser';
import axios from 'axios';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import "../index.css";

const Perdu = () => {
    const  [page, setPage] = useState();
    useEffect(() => {
        axios.get('https://g1.esiee-it.o3creative.fr/wp-json/wp/v2/pages/32?acf_format=standard')
        .then(function (response){
            //handle success
            console.log(response)
            setPage(response.data)
        });
    }, []);
    if(!page) return null;
    return (
        <div>
       <img className='12px' src={page.acf.image}/>
        </div>
    );
};

export default Perdu;