import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Accueil from './Accueil';
import Actualite from './Actualite';
import Header from './Header';
import NavigationBar from './NavigationBar';
import Produit from './Produit';
import Quiz from './Quiz';
import Gagne from './Gagne';
import Perdu from './Perdu';

const MyRouteur = () => {
    return (
                <Router>
                <NavigationBar />
                <Routes>
                  <Route path='/' element={<Accueil />}/>
                  <Route path='Actualite' element={<Actualite />}/>
                  <Route path='Header' element={<Header />}/>
                  <Route path='Produit' element={<Produit />}/>
                  <Route path='Quiz' element={<Quiz />}/>
                  <Route path='Gagne' element={<Gagne />}/>
                  <Route path='Perdu' element={<Perdu />}/>
                </Routes>
              </Router>
    );
};

export default MyRouteur;