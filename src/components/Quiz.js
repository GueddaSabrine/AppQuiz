import React, { useEffect , useState} from 'react';
import axios from 'axios';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import "../index.css";

const Quiz = () => {
    const [quiz, setQuiz] = useState();
    const [rep, setRep] = useState(0);
    var path ="" ;

    useEffect(()=> {
        axios.get('https://g1.esiee-it.o3creative.fr/wp-json/wp/v2/quiz/?acf_format=standard')
        .then(function (response){
            //handle success
            console.log(response)
            setQuiz(response.data)
            
        });
    }, []);

    if(!quiz) return null;
    const handleSubmit =(e)=> {  
            
            
            if (e.target.value === "1") {
                setRep(rep+1)
            }
            else{
                setRep(rep)
            }
            console.log(rep)
    }
    
    return (
        <div>
            <h1> Foot for eat : QUIZ</h1>
            <h4>Tentez de remporter des billets pour l'UEFA EURO 2024</h4>
    {
        quiz.map((item_data, index)=>{
            return <div className="styleCard">
                <Card key={`data_${index}`}>
                <Card.Img variant="top" className='img_a' src={item_data.acf.image_quiz}/>
                <Card.Body>
                    <Card.Title >{item_data.acf.titre}</Card.Title>
                    <Card.Text>
                    {item_data.acf.question}
                    </Card.Text>
                </Card.Body>
                {
                    item_data.acf.reponses.map((reponse, index)=>{
                        return <ListGroup key={index}>
                        <ListGroup.Item>{reponse.reponse} <input type="radio" name="essai" value={reponse.bonne_reponse.length} onClick={handleSubmit}/></ListGroup.Item>
                        </ListGroup>
                    })
                }
                </Card>

                </div>
        })
       
    }
    {
        
    }
   
    if ({rep} == 5) {
        path = "/Gagne"
    } else {
        path ="/Perdu"
    }
    <h1>{rep}</h1>
    <Button variant="primary" href={path}>Fini</Button>

        </div>
    );
};

export default Quiz;