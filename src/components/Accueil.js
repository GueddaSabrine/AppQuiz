import React, { useEffect , useState} from 'react';
import parse from 'html-react-parser';
import axios from 'axios';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import "../index.css";

const Accueil = () => {
    const  [page, setPage] = useState();
    useEffect(() => {
        axios.get('https://g1.esiee-it.o3creative.fr/wp-json/wp/v2/pages/14?acf_format=standard')
        .then(function (response){
            //handle success
            console.log(response)
            setPage(response.data)
        });
    }, []);

    if(!page) return null;
    return (
        // <div>  
        // {/* <h1>Test 2</h1>  */}
        // {/* {page.title.rendered} */}
        // <h1 dangerouslySetInnerHTML={{__html: page.acf.titre}}/>
        // <div dangerouslySetInnerHTML={{__html: page.acf.description}}/>
        //  <img className='12px' src={page.acf.image_accueil}/>
        //  </div>

        <div>
        <Container>
            <Row>
            <h1 dangerouslySetInnerHTML={{__html: page.acf.titre}}/>
            <div dangerouslySetInnerHTML={{__html: page.acf.description}}/>
            <img style={{width: '600px', height:'auto', margin:'50px 350px', padding:'0.3px 10px'}} src={page.acf.image_accueil}/>
            </Row>
        </Container>
        </div>
    );
};

export default Accueil;