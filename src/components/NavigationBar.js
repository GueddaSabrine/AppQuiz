// import React from 'react';
import {Nav, Navbar, NavLink} from "react-bootstrap";
import { Link } from "react-router-dom";

const NavigationBar = () => {
    return (
        <Navbar collapseOnSelect expand="sm" bg="dark" variant="dark">
            <Navbar.Toggle aria-controls="navbarScroll" data-bs-target="#navbarScroll" />
            <Navbar.Collapse id="navbarScroll">
                <Nav>
                    <NavLink eventKey="1" as={Link} to="/">Accueil</NavLink>
                    <NavLink eventKey="2" as={Link} to="/produit">Produits</NavLink>
                    <NavLink eventKey="3" as={Link} to="/quiz">Quiz</NavLink>
                    <NavLink eventKey="4" as={Link} to="/actualite">Actualites</NavLink>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
};

export default NavigationBar;