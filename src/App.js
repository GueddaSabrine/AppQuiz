import logo from './logo.svg';
import './App.css';
import MyRouteur from './components/MyRouteur';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
    <MyRouteur/>
    </div>
  );
}

export default App;
